<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Items;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
 
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    
    const STATUS_SUCCESS = 200;
    const STATUS_ERROR = 201;
    
    
    /**
     * @Get("/products/{condition}", name="list_products",requirements={"condition": "all|in_stock|out_of_stock|min_in_stock"})
     */
    public function listAction(Request $request,$condition='all')
    {
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Items');
        $itemsArr = $repository->geAlltItems($condition);
 
 
        return new Response(json_encode(array('status'=>self::STATUS_SUCCESS,'message'=>'List of products','data'=>$itemsArr)) );
    }
    
    
    /**
     * @Get("/products/get", name="get_product")
     */
    public function getAction(Request $request)
    {   $data['id'] = $request->get('id');
        $repository = $this->getDoctrine()->getRepository('ApiBundle:Items');
        $itemArr = $repository->getItem($data['id']);
        
        if(!empty($itemArr)){
            return new Response(json_encode(array('status'=>self::STATUS_SUCCESS,'message'=>'Product','data'=>$itemArr)) );
        }else{
            return new Response(json_encode(array('status'=>self::STATUS_ERROR,'message'=>'Not found','data'=>$data)) );
        }
        
        

        
    }
    
    
     /**
     * @Post("/products/add", name="add_product")
     */
    
    public function addAction(Request $request){
       
       $data['name'] = $request->get('name');
       $data['ammount'] = (int) $request->get('ammount');
        
        if(!empty($data['name']) && !empty($data['ammount']) && $data['ammount'] > 0){
            $em = $this->getDoctrine()->getManager();
            $items  = new Items();
            $items->setName($data['name']);
            $items->setAmount($data['ammount']);
            $em->persist($items);
            $em->flush();
            $data['id'] = $items->getId();
            return new Response(json_encode(array('status'=>self::STATUS_SUCCESS,'message'=>'Product added','data'=>$data)) );
        }else{
            
            
            return new Response(json_encode(array('status'=>self::STATUS_ERROR,'message'=>'Input error','data'=>$data)));
        }
     
    }
    
     /**
     * @Post("/products/update", name="update_product")
     */
    
    public function updateAction(Request $request ){
        $data['name'] = $request->get('name');
        $data['ammount'] = (int) $request->get('ammount');
        
        
        $data['id'] = $request->get('id');
        
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Items::class)->find($data['id']);
        if($item){
            
            $item->setName($data['name']);
            $item->setAmount($data['ammount']);
            $em->flush();
            
            return new Response(json_encode(array('status'=>self::STATUS_SUCCESS,'message'=>'Product updated','data'=>$data)) );
        }else{
            
            return new Response(json_encode(array('status'=>self::STATUS_ERROR,'message'=>'Not found','data'=>$data)) );
        }
        
    }
    
     /**
     * @Delete("/products/delete", name="delete_product")
     */
    
    public function deleteAction(Request $request){
        $data['id'] = $request->get('id');
        
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository(Items::class)->find($data['id']);
        if($item){
            
           $em->remove($item);
           $em->flush();
            
            return new Response(json_encode(array('status'=>self::STATUS_SUCCESS,'message'=>'Product deleted','data'=>$data)) );
        }else{
            
            return new Response(json_encode(array('status'=>self::STATUS_ERROR,'message'=>'Not found','data'=>$data)) );
        } 
        
    }
}
