<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Unirest\Request as uni;

class ProductController extends Controller {

    const API_URL = 'http://127.0.0.1:8000/';
    const STATUS_SUCCESS = 200;
    const STATUS_ERROR = 201;

    /**
     * @Route("/products/{condition}",name="client_products_list",requirements={"condition":"all|in_stock|out_of_stock|min_in_stock"})
     */
    public function listAction($condition = 'all') {

        $response = uni::get(self::API_URL . 'api_dev.php/api/products/' . $condition);

        $productsArr = $responseArr->data;

        return $this->render('ClientBundle:products:list.html.twig', array('products' => $productsArr));
    }

    /**
     * @Route("/products/add",name="client_products_add")
     */
    public function addAction(Request $request) {

        $message = '';
        $form = $this->createFormBuilder()
                ->add('name', TextType::class)
                ->add('ammount', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Save item'))
                ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $response = uni::post(self::API_URL . 'api_dev.php/api/products/add', '', array('name' => 'Test product_' . mktime(), 'ammount' => 10));
            $responseArr = json_decode($response->body);

            if ($responseArr->status == self::STATUS_SUCCESS) {

                $this->get('session')->getFlashBag()->set('message', 'Product added');

                return $this->redirectToRoute('client_products_list');
            } else {
                $message = 'Error - product not addded';
            }
        }


        return $this->render('ClientBundle:products:form.html.twig', array('clientForm' => $form->createView(), 'message' => $message));
    }

    /**
     * @Route("/products/update",name="client_products_update")
     */
    public function updateAction(Request $request) {
        $message = '';

        if ($request->request->get('id') != null) {

            $response = uni::post(self::API_URL . 'api_dev.php/api/products/update', '', array('id' => 1, 'name' => 'Test product_' . mktime(), 'ammount' => 12));
        }


        $responseArr = json_decode($response->body);

        if ($responseArr->status == self::STATUS_SUCCESS) {

            $this->get('session')->getFlashBag()->set('message', 'Product updated');

            return $this->redirectToRoute('client_products_list');
        } else {
            $message = 'Error - product not addded';
        }


        return $this->render('ClientBundle:products:form.html.twig', array('clientForm' => $form->createView(), 'message' => $message));
    }

    /**
     * @Route("/products/delte/{id}",name="client_products_delete")
     */
    public function deleteAction($id) {

        $response = uni::delete(self::API_URL . 'api_dev.php/api/products/delete', '', array('id' => $id));

        if ($responseArr->status == self::STATUS_SUCCESS) {

            $this->get('session')->getFlashBag()->set('message', 'Product deleted');

            return $this->redirectToRoute('client_products_list');
        } else {
            $this->get('session')->getFlashBag()->set('message', 'Product deleted');

            return $this->redirectToRoute('client_products_list');
        }
    }

    /**
     * @Route("/products/form/{id}",name="client_products_form")
     */
    public function formAction($id = null) {

        if ($id != null) {
            $response = uni::get(self::API_URL . 'api_dev.php/api/products/get', '', array('id' => $id));
            $responseArr = json_decode($response->body);

            if ($responseArr->status == self::STATUS_SUCCESS) {
                $name = $responseArr->data->name;
                $ammount = $responseArr->data->ammount;
                $header = 'Edit product';

                $form = $this->createFormBuilder()
                        ->setAction($this->generateUrl('client_products_update'))
                        ->add('name', TextType::class, array('data' => $name))
                        ->add('ammount', TextType::class, array('data' => $ammount))
                        ->add('id', HiddenType::class, array('data' => $id))
                        ->add('save', SubmitType::class, array('label' => 'Save item'))
                        ->getForm();
            } else {
                $this->get('session')->getFlashBag()->set('message', 'Product not found');

                return $this->redirectToRoute('client_products_list');
            }
        } else {
            $header = 'Add product';
            $form = $this->createFormBuilder()
                    ->add('name', TextType::class, array('attr' => array('class' => 'form-control')))
                    ->add('ammount', TextType::class, array('attr' => array('class' => 'form-control')))
                    ->add('save', SubmitType::class, array('label' => 'Save item', 'attr' => array('class' => 'btn btn-primary')))
                    ->getForm();
        }



        return $this->render('ClientBundle:products:form.html.twig', array('clientForm' => $form->createView(), 'message' => '', 'header' => $header));
    }

}
